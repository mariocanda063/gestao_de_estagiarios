<!doctype html>
<html lang="PT">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" integrity="sha384-OXTEbYDqaX2ZY/BOaZV/yFGChYHtrXH2nyXJ372n2Y8abBhrqacCEe+3qhSHtLjy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
    <title>ITS - INTERN MANAGMENT</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" 
          aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.php">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="?page=novo">Novo Estagiario</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="?page=listar">Ver Estagiarios</a>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled">Em Teste</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

    <div class="container">
      <div class="row">
        <div class="col mt-5">
        <?php
        include("conexao.php");
        switch(@$_REQUEST["page"]){

          case "novo":
            include("novoEstagiario.php");
          break;

          case "listar":
            include("verEstagiario.php");
          break;

          case "editar":
            include("editarEstagiario.php");
          break;

          case "salvar":
              include("salvarEstagiario.php");
          default:
           # print "<h1>As melhores maquinas, encontra aqui</h1>";
        }
      
      ?>
        </div>
      </div>
    </div>

       <footer style=" background-color: #c6caff; text-align: center;
    padding: 10px;
    margin-top: 30px;
    position: absolute;
    bottom: 0;
    width: 100%;">
        <div style="color: black;">ITS - &copy; Mário Canda</div>
       </footer>
        <script src="/boostrap/js/bootstrap.bundle.min.js"></script>
  </body>
</html>