<h1>Novo Estagiario</h1>
<br>
<script src="js/script.js" defer></script>
<form method="POST" action="?page=salvar" class="row g-3">
<input type="hidden" name="acao" value="cadastrar">

  <div class="col-md-6">
    <label for="name" class="form-label">Nome Completo</label>
    <input type="text" minlength="3" class="form-control" id="name" placeholder="Mário Canda" name="txtNome" required>
  </div>

  <div class="col-md-5">
    <label for="inputState" class="form-label">Departamento</label>
    <select id="inputState" class="form-select" name="genero">
      <option selected>Selecione um...</option>
      <option>Programacao</option>
      <option>Marketing</option>
      <option>Contabilidade</option>
      <option>RH</option>
    </select>
  </div>

 
  <div class="col-md-5">
    <label for="email" class="form-label">Email</label>
    <input type="email" class="form-control" id="email" name="txtEmail" placeholder="marioc@gmail.com" required>
  </div>

  <div class="col-md-8">
    <label for="inputAddress" class="form-label">Endereço</label>
    <input type="text" class="form-control" id="inputAddress" placeholder="Av.24 de Julho, nº 3922" name="txtEndereco" required spellcheck="true">
  </div>

  <div class="col-md-4">
      <label for="cell" class="form-label">Celular</label> 
      <input type="tel" class="form-control" id="autoSizingInputGroup" name="txtCell" maxlength="9" required placeholder="84 123 4567" title="ex:841234567">
  </div>

  <div class="col-md-3">
    <label for="inputState" class="form-label">Genero</label>
    <select id="inputState" class="form-select" name="genero">
      <option selected>Escolha</option>
      <option>Masculino</option>
      <option>Femenino</option>
    </select>
  </div>

  <div class="col-md-4">
    <label for="empresa" class="form-label">Data de Nascimento</label>
    <input type="date" class="form-control" id="dataNascimento" name="dataNascimento" spellcheck="true">
  </div>

  <div class="col-md-2">
    <label for="dias" class="form-label">Duraçao (Dias)</label>
    <input type="number" class="form-control" id="dias" min="5" name="txtDias">
  </div>
  
  <div class="col-12">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck" checked disabled required>
      <label class="form-check-label" for="gridCheck">
        Ao se cadastrar , aceita em partilhar as suas info.
      </label>
    </div>
  </div>
  <div class="col-12">
    <button type="submit" class="btn btn-outline-success">Cadastro</button>
  </div>
</form>